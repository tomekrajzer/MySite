from django.db import models


class Project(models.Model):

    creation_date = models.DateField(auto_now=True)
    is_live = models.BooleanField(default=False)
    project_title = models.CharField(max_length=250)
    project_description = models.TextField(blank=True)
    project_image = models.ImageField(blank=True)
    project_url = models.URLField(blank=True, null=True)
    repositorium_url = models.URLField(blank=True, null=True)
    video = models.FileField(upload_to='uploads/')

    def __str__(self):
        return self.project_title